using UnityEngine;

public class EmptySpace : MonoBehaviour {
  public Sprite over;
  Sprite normal;
  SpriteRenderer sr;
  public SpriteRenderer buildingSprite;
  Building building;
  public AudioClip clip;
  public AudioClip errorClip;

  private void Awake() {
    sr = GetComponent<SpriteRenderer>();
    normal = sr.sprite;
  }

  private void OnMouseEnter() {
    sr.sprite = over;
  }

  private void OnMouseExit() {
    sr.sprite = normal;
  }

  public void setBuilding (GameObject building, Sprite sprite) {
    this.building = building.GetComponent<Building>();
    buildingSprite.sprite = sprite;
  }

  private void OnMouseDown() {
    Building b = building.GetComponent<Building>();
    if (GameController.I.energy >= b.energyCost && GameController.I.money >= b.moneyCost) {
      Instantiate(building, transform.position, Quaternion.identity);
      GameController.I.playMode();
      GameController.I.removeEmptySpace(this);
      GameController.I.energy -= b.energyCost;
      GameController.I.money -= b.moneyCost;
      GameController.I.playAudio(clip);
      Destroy(gameObject);
    } else {
      GameController.I.floatText(transform.position, "Not enough resources", Color.red, null);
      GameController.I.playAudio(errorClip);
    }
  }
}