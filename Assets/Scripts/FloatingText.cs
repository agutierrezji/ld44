﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour {
  public int time = 2;
  public float speed = 0.1f;
  public float fadeTime = 1f;
  public Image image;

  string _text;
  public string text {
    get => _text;
    set {
      _text = value;
      tm.text = value;
    }
  }

  Color _color;
  public Color color {
    get => _color;
    set {
      _color = value;
      tm.color = value;
    }
  }

  Sprite _sprite;
  public Sprite sprite {
    get => _sprite;
    set {
      image.gameObject.SetActive(image != null);
      _sprite = value;
      image.sprite = sprite;
    }
  }

  TextMeshProUGUI tm;

  void Awake() {
    tm = GetComponent<TextMeshProUGUI>();
  }

  void Start() {
    Invoke("destroy", time);
  }

  void Update() {
    transform.position += Vector3.up * speed * Time.deltaTime;
  }

  void destroy() {
    StartCoroutine(fade(fadeTime));
  }

  IEnumerator fade(float aTime) {
    float alpha = tm.color.a;
    for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime) {
      Color newColor = new Color(tm.color.r, tm.color.g, tm.color.b, Mathf.Lerp(alpha, 0, t));
      Color imageColor = new Color(image.color.r, image.color.g, image.color.b, Mathf.Lerp(alpha, 0, t));
      tm.color = newColor;
      image.color = imageColor;
      yield return null;
    }
    Destroy(gameObject);
 }


}
