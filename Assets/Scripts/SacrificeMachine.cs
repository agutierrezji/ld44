using UnityEngine;

public class SacrificeMachine : Building {

  public ParticleSystem bloodEmiter;
  public AudioClip clip;

  public override void addSlave(Slave slave) {
    GameController gm = GameController.I;
    int moneyToGive = 5;
    gm.money += moneyToGive;
    bloodEmiter.Play();
    gm.playAudio(clip);
    gm.floatText(transform.position, $"+{moneyToGive}", Color.white,  gm.moneySprite);
    gm.dieSlave(slave);
  }

  public override void removeSlave(Slave slave){}
}