using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using System.Linq;

public class Lepus {
   /**
   * Execute action from start to end
   */
  public static void fireTimes (int start, int end, Action<int> func) {
    fireTimes(start, end, func, i => false);
  }

  /**
   * Execute action from start to end. Stops if breakFunc returns true
   */
  public static void fireTimes (int start, int end, Action<int> func, Func<int, bool> breakFunc) {
    int i = start; 

    Action step = () => { 
      i = start < end 
        ? i + 1 
        : i - 1; 
    };
    Func<bool> check = () => 
      start < end 
        ? i <= end 
        : i >= end;

    for (; check(); step()) {
      if (breakFunc(i)) break;
      func(i);
    }
  }

  /**
  * Get value from a dictionary. If it doesn't exists returns default type value
   */
  public static T get<S, T> (Dictionary<S, T> dic, S key) {
    return getOr(dic, key, default(T));
  }

  /**
  * Get value from a dictionary. If it doesn't exists return default
   */
  public static T getOr<S, T>(Dictionary<S, T> dic, S key, T def) {
    return dic.ContainsKey(key)
      ? dic[key]
      : def;
  }

  /**
  * Fire function after time
  */
  public static Coroutine fire (Action action, float time, MonoBehaviour mono) {
      return mono.StartCoroutine(_fire(action, time));
  }

  private static IEnumerator _fire (Action action, float time) {
      yield return new WaitForSeconds(time);
      action();
  }
}