using UnityEngine;
using System.Linq;
using System;

public class WindMill : Building {

  public Slave[] slaves;

  public Sprite[] fillSprites;

  int fillStatus = 0;

  float TIME_TO_PRODUCE = 5f;

  public GameObject spawnPoint;

  SpriteRenderer sr;
  
  void Awake() {
    slaves = new Slave[3];
    sr = GetComponent<SpriteRenderer>();
  }

  void Start() {
    InvokeRepeating("step", TIME_TO_PRODUCE, TIME_TO_PRODUCE);  
  }

  public override void addSlave (Slave slave) {
    int space = firstEmptySpace();
    if (space == -1) return;

    slaves[space] = slave;
    slave.gameObject.SetActive(false);
    slave.work(this);
    fillStatus++;
    sr.sprite = fillSprites[fillStatus];
  }

  public override void removeSlave(Slave slave){
    int index = Array.IndexOf(slaves, slave);
    slaves[index] = null;
    fillStatus--;
    sr.sprite = fillSprites[fillStatus];
    slave.gameObject.SetActive(true);
    slave.transform.position = spawnPoint.transform.position;
    slave.workingPlace = null;
    slave.work(null);
  }

  int firstEmptySpace () {
    for (int i = 0; i < slaves.Length; i++) {
      if (!slaves[i]) return i;
    }
    return -1;
  }

  int firstOcuppiedSpace () {
    for (int i = 0; i < slaves.Length; i++) {
      if (slaves[i]) return i;
    }
    return -1;
  }

  public int slavesCount () {
    return slaves.Count(x => x != null);
  }

  private void OnMouseDown() {
    int index = firstOcuppiedSpace();
    if (index == -1) return;
    removeSlave(slaves[index]);
  }

  public int slavesNum () {
    return slaves.Count(x => x != null);
  }

  void step () {
    GameController gm = GameController.I;
    int food = slavesNum();
    if (food == 0) return;
    gm.food += food;
    gm.floatText(transform.position, $"+{food}", Color.white,  gm.foodSprite);
  }
}