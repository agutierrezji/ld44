using UnityEngine;

public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour {
  // Singleton Instance
  public static T I {get; private set;}

  protected virtual void Awake() {
    if (I == null) {
      I = this as T;
    } else {
      Destroy(this);
    }
  }

  public static void destroyInstance () {
    I = null;
  }
}
