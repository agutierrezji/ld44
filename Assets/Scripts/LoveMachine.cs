using UnityEngine;
using System.Linq;
using System;

public class LoveMachine : Building {
  public Slave[] slaves;
  public GameObject[] finalPoints;

  public int timeToLove = 5;

  public ParticleSystem hearthEmiter;
  public AudioClip clip;

  void Awake() {
    slaves = new Slave[2];
  }

  public override void addSlave (Slave slave) {
    int space = firstEmptySpace();
    if (space == -1) return;

    slaves[space] = slave;
    slave.gameObject.SetActive(false);
    slave.work(this);
    
    if (slavesCount() == 2) startLoving();
  }

  public override void removeSlave (Slave slave){
    int index = Array.IndexOf(slaves, slave);
    if (index != -1) {
      slaves[index] = null;
    }
  }

  int firstEmptySpace () {
    for (int i = 0; i < slaves.Length; i++) {
      if (!slaves[i]) return i;
    }
    return -1;
  }

  int firstOcuppiedSpace () {
    for (int i = 0; i < slaves.Length; i++) {
      if (slaves[i]) return i;
    }
    return -1;
  }

  public int slavesCount () {
    return slaves.Count(x => x != null);
  }

  void startLoving () {
    hearthEmiter.Play();
    Invoke("loved", timeToLove);
  }

  void loved () {
    for (int i = 0; i < slaves.Length; i++) {
      if (slaves[i]) {
        slaves[i].gameObject.SetActive(true);
        slaves[i].transform.position = finalPoints[i].transform.position;
        slaves[i].workingPlace = null;
        slaves[i] = null;
      }
    }

    Slave slave = Instantiate(
      GameController.I.slavePrefab,
      finalPoints[2].transform.position,
      Quaternion.identity
    ).GetComponent<Slave>();

    GameController.I.newSlave(slave);
    GameController.I.playAudio(clip);
    hearthEmiter.Stop();
  }

}