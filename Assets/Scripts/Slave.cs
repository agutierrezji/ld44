using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Slave : MonoBehaviour {
  public static int STAND_STATE = 0;
  public static int CAUGHT_STATE = 1;
  public static int RUN_STATE = 2;
  public Building workingPlace;
  string STATE = "state";

  public AudioClip clip;

  Animator anim;
  Rigidbody2D rb;
  Collider2D col;

  void Awake() {
    anim = GetComponent<Animator>();
    rb = GetComponent<Rigidbody2D>();
    col = GetComponent<Collider2D>();
  }

  void OnMouseDown() {
    GameController.I.playAudio(clip);
  }

  void OnMouseDrag() {
    anim.SetInteger(STATE, CAUGHT_STATE);
    if(workingPlace) {
      workingPlace.removeSlave(this);
      workingPlace = null;
    }

    Vector3 mousePosition = GameController.I.mainCamera.ScreenToWorldPoint(Input.mousePosition);
    rb.position = mousePosition;
  }

  void Update() {
    if (Input.GetMouseButtonUp(0) && !workingPlace) {
      Collider2D[] colls = new Collider2D[3];
      int contacts = col.GetContacts(colls);
      Collider2D element = colls.FirstOrDefault(x => x && x.GetComponent<Building>() != null);

      if (element) {
        element.GetComponent<Building>().addSlave(this);
      } else {
        anim.SetInteger(STATE, STAND_STATE);
      }

    }
  }

  public void enableCollider(bool enable) {
    col.enabled = enable;
  }

  public void setAnimation (int animation) {
    anim.SetInteger(STATE, animation);
  }

  public void work(Building building) {
    workingPlace = building;
  }

}