using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class BuildButton : MonoBehaviour {
  
  public Sprite over;
  public GameObject building;
  public SpriteRenderer buildingSprite;
  [TextArea]
  public string description;
  public AudioClip clip;
  Sprite normal;
  SpriteRenderer sr;
  public bool reset = false;

  private void Awake() {
    sr = GetComponent<SpriteRenderer>();
    normal = sr.sprite;
  }

  private void OnMouseEnter() {
    sr.sprite = over;
    if (building) {
      Building b = building.GetComponent<Building>();
      GameObject canvas = GameController.I.descriptionCanvas;
      canvas.SetActive(true);
      canvas.GetComponentInChildren<TextMeshProUGUI>().text = description.Replace("$money$", b.moneyCost.ToString()).Replace("$energy$", b.energyCost.ToString());
    }
    
  }

  private void OnMouseExit() {
    GameController.I.descriptionCanvas.SetActive(false);
    sr.sprite = normal;
  }

  private void OnMouseDown() {
    if (reset) {
      SceneManager.LoadScene( SceneManager.GetActiveScene().name );
      return;
    }
    GameController.I.playAudio(clip);
    if (building) {
      GameController.I.contructionMode(building, buildingSprite.sprite);
    } else {
      GameController.I.playMode();
    }
  }

}