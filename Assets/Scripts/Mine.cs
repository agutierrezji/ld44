using UnityEngine;
using System.Linq;
using System;

public class Mine : Building {
  public Slave[] slaves;
  public GameObject[] runPoints;

  public int timePerStep = 120;
  int currentTime;

  void Awake() {
    slaves = new Slave[3];
    currentTime = timePerStep;
  }

  void Start() {
    InvokeRepeating("step", 1f, 1f);  
  }

  public override void addSlave (Slave slave) {
    int space = firstEmptySpace();
    if (space == -1) return;

    slaves[space] = slave;
    slave.gameObject.SetActive(false);
    slave.work(this);
  }

  public override void removeSlave (Slave slave) {
    int index = Array.IndexOf(slaves, slave);
    slaves[index].gameObject.SetActive(true);
    slaves[index] = null;
  }

  int firstEmptySpace () {
    for (int i = 0; i < slaves.Length; i++) {
      if (!slaves[i]) return i;
    }
    return -1;
  }

  int firstOcuppiedSpace () {
    for (int i = 0; i < slaves.Length; i++) {
      if (slaves[i]) return i;
    }
    return -1;
  }

  public int slavesNum () {
    return slaves.Count(x => x != null);
  }

  void step () {
    GameController.I.money += slavesNum();
  }

  void OnMouseDown() {
    int index = firstOcuppiedSpace();
    if (index == -1) return;
    Slave slave = slaves[index];
    slave.setAnimation(Slave.CAUGHT_STATE);
    slave.work(null);
    removeSlave(slave);
  }

}