using UnityEngine;

public class SlavesMarket : Building {
  public int SLAVE_PRICE;
  public GameObject slavePrefab;
  public GameObject point;
  public AudioClip clipBuy;
  public AudioClip clipError;
  public override void removeSlave(Slave s){}
  public override void addSlave(Slave s){}
 
  void OnMouseDown() {
    GameController gm = GameController.I;
    if (gm.money >= SLAVE_PRICE) {
      gm.money -= SLAVE_PRICE;
      gm.floatText(transform.position, $"-{SLAVE_PRICE}", Color.red,  gm.moneySprite);
      GameController.I.playAudio(clipBuy);
      createSlave();
    } else {
      gm.floatText(transform.position, $"Not enough", Color.red,  gm.moneySprite);
      GameController.I.playAudio(clipError);
    }
  }

  void createSlave () {
    Slave slave = Instantiate(slavePrefab, point.transform.position, Quaternion.identity).GetComponent<Slave>();
    GameController.I.newSlave(slave);
  }
}