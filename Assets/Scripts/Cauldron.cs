using UnityEngine;

public class Cauldron : Building {

  public ParticleSystem bloodEmiter;
  public AudioClip clip;

  public override void addSlave(Slave slave) {
    GameController gm = GameController.I;
    gm.food += 5;
    bloodEmiter.Play();
    gm.playAudio(clip);
    gm.floatText(transform.position, "+5", Color.white,  gm.foodSprite);
    gm.dieSlave(slave);
  }

  public override void removeSlave(Slave slave){}
}