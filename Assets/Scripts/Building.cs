using UnityEngine;

public abstract class Building : MonoBehaviour {
  public int energyCost;
  public int moneyCost;
  public abstract void addSlave (Slave slave);
  public abstract void removeSlave (Slave slave);
}