﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEngine.UI;

public class GameController : MonoSingleton <GameController> {
    public Camera mainCamera;

    public GameObject slavePrefab;
    public GameObject floatTextPrefab;
    public GameObject canvas;
    public float eatFoodTime;
    public float eatFoodRate;
    public GameObject eatFoodPosition;
    public GameObject descriptionCanvas;
    public GameObject winCanvas;
    public TextMeshProUGUI timer;
    float time;
    bool lastTurnHungry = false;

    public List<Slave> slavesList;

    public GameObject[] emptySpaces;

    public int slaves = 0;
    public int money = 0;
    public int energy = 0;
    public int food = 0;

    public TextMeshProUGUI slavesText;
    public TextMeshProUGUI moneyText;
    public TextMeshProUGUI energyText;
    public TextMeshProUGUI foodText;

    public Sprite slavesSprite;
    public Sprite moneySprite;
    public Sprite energySprite;
    public Sprite foodSprite;
    public Sprite skullSprite;

    protected new void Awake() {
        base.Awake();
        slavesList = new List<Slave>();    
    }

    void Start() {
        slavesList = Object.FindObjectsOfType<Slave>().ToList();
        slaves = slavesList.Count;
        InvokeRepeating("eat", eatFoodTime, eatFoodTime);
        time = Time.deltaTime;
    }

    void Update() {
        slavesText.SetText(slaves.ToString());
        energyText.SetText(energy.ToString());
        foodText.SetText(food.ToString());
        moneyText.SetText(money.ToString());
        updateTimer();
    }

    public void floatText(Vector3 position, string text, Color color, Sprite sprite) {
        Vector3 _position = new Vector3(position.x, position.y, -9);
        FloatingText ft = Instantiate(floatTextPrefab, _position, Quaternion.identity, canvas.transform).GetComponent<FloatingText>();
        ft.text = text;
        ft.color = color;
        ft.sprite = sprite;
        ft.GetComponentInChildren<Image>().gameObject.SetActive(sprite != null);
    }

    public void win () {
        var textCanvas = winCanvas.GetComponentInChildren<TextMeshProUGUI>();
        winCanvas.SetActive(true);
        string minutes = Mathf.Floor(time / 60).ToString("00");
        string seconds = (time % 60).ToString("00");
        textCanvas.text = textCanvas.text.Replace("$time$", $"{minutes}:{seconds}");
        timer = null;
        Invoke("closegame", 10);
    }

    void closegame () => Application.Quit();

    void eat () {
        if (slavesList.Count <= 0) return;

        int eatenFood = Mathf.FloorToInt(slaves * eatFoodRate);
        if (eatenFood <= 0) {
            eatenFood = 1;
        }
        food -= eatenFood;
        if (food >= 0) {
            lastTurnHungry = false;
            floatText(eatFoodPosition.transform.position, $"-{eatenFood}", Color.red, foodSprite);
        } else {
            food = 0;
            floatText(eatFoodPosition.transform.position, $"-{eatenFood}!!!", Color.red, foodSprite);
            if (lastTurnHungry) {
                int index = Random.Range(0, slavesList.Count);
                dieSlave(slavesList[index], true);
            } else {
                lastTurnHungry = true;
            }
        }
    }

    public void dieSlave (Slave slave) => dieSlave(slave, false);

    public void dieSlave (Slave slave, bool skull) {
        slavesList.Remove(slave);
        slaves--;
        if (slave.workingPlace) {
            slave.workingPlace.removeSlave(slave);
        }
        if (skull) {
            floatText(slave.transform.position, "", Color.white, skullSprite);
        }
        Destroy(slave.gameObject);
    }

    public void newSlave (Slave slave) {
        slaves++;
        slavesList.Add(slave);
    }

    public void contructionMode (GameObject building, Sprite sprite) {
        foreach (GameObject space in emptySpaces) {
            if (!space) continue;
            space.SetActive(true);
            space.GetComponent<EmptySpace>().setBuilding(building, sprite);
        }
    }

    public void playMode () {
        foreach (GameObject space in emptySpaces) {
            if (!space) continue;
            space.SetActive(false);
        }
    }

    public void removeEmptySpace (EmptySpace es) {
        int index = System.Array.IndexOf(emptySpaces, es.gameObject);
        emptySpaces[index] = null;
    }

    void updateTimer () {
        if (!timer) return;
        time += Time.deltaTime;
        string minutes = Mathf.Floor(time / 60).ToString("00");
        string seconds = (time % 60).ToString("00");
        timer.text = $"{minutes}:{seconds}";
    }

    public void playAudio (AudioClip clip) {
        AudioSource audio = this.GetComponent<AudioSource>();
        audio.PlayOneShot(clip);
    }
}
