using UnityEngine;
using System.Linq;
using System;

public class EnergyWheel : Building {
  public Slave[] slaves;
  public GameObject[] runPoints;

  string MOVING = "move";
  Animator anim;

  void Awake() {
    slaves = new Slave[3];
    anim = GetComponentInChildren<Animator>();
  }

  void Start() {
    InvokeRepeating("step", 2f, 2f);  
  }

  void Update() {
    anim.SetBool(MOVING, slavesNum() != 0);
  }

  public override void addSlave (Slave slave) {
    int space = firstEmptySpace();
    if (space == -1) return;

    slaves[space] = slave;
    slave.transform.position = runPoints[space].transform.position;
    slave.setAnimation(Slave.RUN_STATE);
    slave.work(this);
  }

  public override void removeSlave (Slave slave) {
    int index = Array.IndexOf(slaves, slave);
    slaves[index] = null;
  }

  int firstEmptySpace () {
    for (int i = 0; i < slaves.Length; i++) {
      if (!slaves[i]) return i;
    }
    return -1;
  }

  public int slavesNum () {
    return slaves.Count(x => x != null);
  }

  void step () {
    GameController gm = GameController.I;
    int energy = slavesNum() * 2;
    if (energy == 0) return;
    gm.energy += energy;
    gm.floatText(transform.position, $"+{energy}", Color.white,  gm.energySprite);
  }

}